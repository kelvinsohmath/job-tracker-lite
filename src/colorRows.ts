// import { uniqueIDs } from './stores.js';
// import { get, set } from 'svelte/store';


const jobClasses = ["scheduled", "delivered", "paid", "cancelled"];
const jobBackgroundClasses = ["scheduled-background", "delivered-background", "paid-background", "cancelled-background"];

function colorRows(removeClass = false) {
  const rows = document.querySelector('.table').rows;
  const l = rows.length;
  for (let i = 1; i < l; i++) {
    const row = rows[i].childNodes;
    const l2 = row.length;
    // find job status
    let jobStatus = 0; // scheduled by default
    if (row[0].innerHTML.trim() == "delivered") {
      jobStatus = 1;
    } else if (row[0].innerHTML.trim() == "paid") {
      jobStatus = 2;
    } else if (row[0].innerHTML.trim() == "cancelled") {
      jobStatus = 3;
    }
    // change color of status "button"
    if (removeClass) {
      removeColors(row[0], false);
    }
    row[0].classList.add(jobClasses[jobStatus]);
    // determine where each data set went in the table
    // const uniqueID = row[1].innerHTML.trim() + row[2].innerHTML.trim();
    // color rest of row
    for (let j = 1; j < l2; j++) {
      if (removeClass) {
        removeColors(row[j]);
      }
      row[j].classList.add(jobBackgroundClasses[jobStatus]);
    }
  }
}
function removeColors(cell: any, backgroundMode = true) {
  if (backgroundMode) {
    cell.classList.remove("paid-background");
    cell.classList.remove("scheduled-background");
    cell.classList.remove("delivered-background");
    cell.classList.remove("cancelled-background");
  } else {
    cell.classList.remove("paid");
    cell.classList.remove("scheduled");
    cell.classList.remove("delivered");
    cell.classList.remove("cancelled");
  }
}

export{colorRows}