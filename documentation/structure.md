App.svelte
|- CurrentJobsTable.svelte : table view
  |- Svelte-table
  |- JobDialog.svelte : edit job dialog
    |- SMUI Button, Dialog, Chip, TextField, Switch, FormField
    |- colorRows.ts
    |- stores.js
  |- CancelDialog.svelte
    |- SMUI Button, Dialog, TextField, Switch, FormField
    |- colorRows.ts
    |- stores.js
  |- colorRows.ts
  |- stores.js
|- AddJobButton.svelte : opens add job dialog
  |- SMUI Button
  |- JobDialog.svelte : add job dialog
    |- see JobDialog above in CurrentJobsTable.svelte
|- JobsView : drawer separated by jobs
  |- stores.js

stores.js
--> $currentJobData
  --> CancelDialog.svelte
  --> JobDialog.svelte
  --> JobsView.svelte
--> $uniqueIDs
--> $dataPositions

colorRows.ts
--> CurrentJobsTable.svelte