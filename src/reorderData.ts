import { currentJobData } from './stores.js';
import { get, set } from 'svelte/store';

function reorderData() { // reorderData by name
  let jobData = get(currentJobData);
  jobData.sort((j1, j2) => {
    if (j1.date < j2.date) {
      return -1;
    } else if (j1.date > j2.date) {
      return 1
    } else {
      if (j1.name < j2.name) {
        return -1
      } else if (j1.name > j2.name) {
        return 1
      }
    }
    return 0;
  })
  jobData.forEach((job, i) => {
    job.id = i;
  })
  currentJobData.set(jobData);
}


export{reorderData}