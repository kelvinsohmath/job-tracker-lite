import { currentJobData} from './stores.js';
import { colorRows } from './colorRows';
import { reorderData } from './reorderData';
import { tick } from 'svelte';
import { get } from 'svelte/store';

async function removeJobs(name, startDate) { // remove all pending jobs after startDate
  let jobData = get(currentJobData);
  let jobDataCopy = [];
  jobData.forEach((row, i) => {
    if (!(row.name == name && row.date > startDate && row.status==0)) {
      jobDataCopy.push(row);
    }
  })
  jobDataCopy.forEach((_, i) => {  // reset IDs
    jobDataCopy[i].id = i;
  })
  currentJobData.set(jobDataCopy);  
  await tick();
  colorRows(true); //recolor table
}

async function addJobs(name, startDate, rate, daysMultiple = 7) { // add pending jobs after startDate in 7 days interval for current+next month
  let jobData = get(currentJobData);
  let newID = jobData[jobData.length - 1].id;
  let newJobs = []
  const currentDate = new Date();
  const jobDate = new Date(startDate);
  let newJobDate = new Date(jobDate);
  newJobDate.setDate(jobDate.getDate() + daysMultiple);
  while (newJobDate.getMonth() == currentDate.getMonth()-1 || newJobDate.getMonth() == currentDate.getMonth() || newJobDate.getMonth() == currentDate.getMonth() + 1 || (newJobDate.getMonth() == 0 && currentDate.getMonth() == 11)) {
    newID++;
    newJobs.push({ id: newID, status: 0, date: newJobDate.toISOString().slice(0, 10), name: name, rate: rate, comments: "" })
    newJobDate.setDate(newJobDate.getDate() + daysMultiple);
  }
  jobData = [...jobData, ...newJobs]
  currentJobData.set(jobData);
  //reorderData();
  await tick();
  if (document.querySelector('.table')) {
    colorRows (true); //recolor table
  }
}

async function jobPay(name, allMode = true) {
  let jobData = get(currentJobData);
  const currentDate = new Date().toISOString().slice(0, 10);
  let jobCount = 0;
  jobData.forEach((job, i) =>{
    if (job.name == name && job.status == 1 && job.date <= currentDate) {
      if (allMode || jobCount < 4) {
        jobData[i].status = 2;
      }
      jobCount++;
    }
  })    
  currentJobData.set(jobData);
  await tick();
  colorRows (true); //recolor table
}

export{removeJobs, addJobs, jobPay}