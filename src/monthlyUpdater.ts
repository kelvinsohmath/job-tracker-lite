import { addJobs } from "./addRemoveJobs";

const currentDate = new Date();
const currentMonth = currentDate.toISOString().slice(0, 7);
let oneYearAgo = new Date();
oneYearAgo.setFullYear(currentDate.getFullYear() - 1);
const prevYear = oneYearAgo.toISOString().slice(0, 10);

function moveToArchive(current, archive) {
  const l = current.length;
  let newArchive = [], indicesToRemove = [];
  for (let i = 0; i < l; i++) {
    let job = current[i];
    if (job.date.slice(0, 7) < currentMonth) {
      if (job.status == 2) { // paid
        indicesToRemove.push(i);
        newArchive.push(job);
      } else if (job.status == 3) { // cancelled
        job.rate = 0;
        indicesToRemove.push(i);
        newArchive.push(job);
      }
    } else {
      break; // since jobs are reordered by date, need not consider following elements
    }
  }
  const newCurrentJobs = current.filter((_, i) => !indicesToRemove.includes(i));
  return [newCurrentJobs, [...archive, ...newArchive]];
}

function pushNewJobs(current, recurring) {
  recurring.forEach(jobName => {
    // find last index of job
    let lastIndex = -1;
    current.forEach((job, index) => {
      if (job.name == jobName) {
        lastIndex = index;
      }
    })
    if (lastIndex != -1) {
      addJobs(jobName, current[lastIndex].date, current[lastIndex].rate);
    }
  })
}

function moveToHistory(archive, history) {
  const l = archive.length;
  let newHistory = [], indicesToRemove = [];
  for (let i = 0; i < l; i++) {
    let job = archive[i];
    if (job.date < prevYear) {
        indicesToRemove.push(i);
        // TODO 
    } else {
      break; // since jobs are reordered by date, need not consider following elements
    }
  }
}

export{ moveToArchive, pushNewJobs}