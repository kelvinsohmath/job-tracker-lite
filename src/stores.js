import { writable, derived } from 'svelte/store';

// dates for example illustration
const currentDate = new Date();
let dMinusSeven = new Date();
let dMinusTwo = new Date();
let dPlusOne = new Date();
let dPlusEight = new Date();
dMinusSeven.setDate(currentDate.getDate() - 7);
dMinusTwo.setDate(currentDate.getDate() - 2);
dPlusOne.setDate(currentDate.getDate() + 1);
dPlusEight.setDate(currentDate.getDate() + 8);

// TODO: only for testing
let dMinusOneMonth = new Date();
dMinusOneMonth.setMonth(currentDate.getMonth() - 1);
let dMinusOneMonthPlus = new Date(dMinusOneMonth);
dMinusOneMonthPlus.setDate(dMinusOneMonth.getDate()-3)

const starterData = [
  { id: 0, status: 3, date: dMinusSeven.toISOString().slice(0,10), name: "Student A", rate: 150,  comments: "example one-off cancelled" },
  { id: 1, status: 2, date: dMinusSeven.toISOString().slice(0,10), name: "Student B", rate: 140,  comments: "example recurring paid" },
  { id: 2, status: 1, date: dMinusTwo.toISOString().slice(0,10), name: "Student C", rate: 200,  comments:   "example one-off delivered" },
  { id: 3, status: 0, date: currentDate.toISOString().slice(0,10), name: "Student D", rate: 120,  comments: "example one-off scheduled" },
  { id: 4, status: 1, date: currentDate.toISOString().slice(0,10), name: "Student B", rate: 160,  comments: "example recurring delivered" },
  { id: 5, status: 2, date: currentDate.toISOString().slice(0,10), name: "Student A", rate: 150,  comments: "example one-off paid" },
  { id: 6, status: 3, date: currentDate.toISOString().slice(0,10), name: "Student C", rate: 120,  comments: "example one-off cancelled" },
  { id: 7, status: 3, date: dPlusOne.toISOString().slice(0,10), name: "Student D", rate: 160,  comments: "example one-off cancelled" },
  { id: 8, status: 0, date: dPlusEight.toISOString().slice(0,10), name: "Student C", rate: 200,  comments: "example one-off scheduled" },
  { id: 9, status: 3, date: dMinusOneMonth.toISOString().slice(0,10), name: "Student C", rate: 200,  comments: "old testing" } // TODO: remove after testing
];
const starterRecurring = ["Student B"];
const starterMonth = "2021-04"; // TODO: testing
//const starterMonth = currentDate.toISOString().slice(0, 7); // TODO: use for production
const starterArchiveData = [ // TODO: dates
  { id: 4, status: 2, date: dMinusSeven.toISOString().slice(0, 10), name: "Student B", rate: 160, comments: "" },
  { id: 0, status: 2, date: dMinusOneMonth.toISOString().slice(0, 10), name: "Student A", rate: 160, comments: "" }, 
  { id: 1, status: 2, date: dMinusOneMonth.toISOString().slice(0, 10), name: "Student A", rate: 160, comments: "" },
  { id: 2, status: 2, date: dMinusOneMonth.toISOString().slice(0, 10), name: "Student A", rate: 160, comments: "" },
  { id: 1, status: 2, date: dMinusOneMonthPlus.toISOString().slice(0, 10), name: "Student A", rate: 0, comments: "" }
];
const starterYearlyArchive = [
  {
    year: 2019,
    jobs: [
      { name: "Student Z", paid: [0, 500, 600, 700, 600, 700, 800, 1000, 500, 600, 400, 0] },
      { name: "Student Y", paid: [2000, 500, 1600, 2700, 3600, 2700, 4800, 1000, 5500, 600, 2400, 1000] },
    ]
  },
  {
    year: 2020,
    jobs: [
      { name: "Student X", paid: [1000, 800, 2600, 1700, 2600, 700, 900, 5000, 2500, 700, 200, 2000] },
      { name: "Student Y", paid: [2000, 500, 1600, 2700, 3600, 2700, 4800, 1000, 5500, 600, 2400, 1000] },
    ]
  }
];

// currentJobData: sync with localStorage
const currentJobDataStorage = JSON.parse(localStorage.getItem("currentJobData"));
export const currentJobData = writable( notAnArray(currentJobDataStorage) ? starterData : currentJobDataStorage);
currentJobData.subscribe(value => {
  const sortedJobs = reorderData(value)
  localStorage.setItem("currentJobData", value ? JSON.stringify(sortedJobs) : JSON.stringify(starterData));
})
// recurring jobs: sync with localStorage
const recurringJobsStorage = JSON.parse(localStorage.getItem("recurringJobs"));
export const recurringJobs = writable(recurringJobsStorage ? recurringJobsStorage : starterRecurring);
recurringJobs.subscribe(value => {
  localStorage.setItem("recurringJobs", value ? JSON.stringify(value) : JSON.stringify(starterRecurring));
})
// uniqueIDs: derived from currentJobData
export const uniqueIDs = derived(
  currentJobData,
  $currentJobData => {
    let idArray = [];
    $currentJobData.forEach((row) => {
      idArray.push(`${row.date.slice(5, 7)}/${row.date.slice(8, 10)}${row.name}`);
    });
    return idArray;
  }
)

// last month updated: sync with localStorage
const lastMonthUpdatedStorage = JSON.parse(localStorage.getItem("lastMonthUpdated"));
export const lastMonthUpdated = writable(lastMonthUpdatedStorage ? lastMonthUpdatedStorage : starterMonth);
lastMonthUpdated.subscribe(value => {
  localStorage.setItem("lastMonthUpdated", value ? JSON.stringify(value) : JSON.stringify(starterMonth));
})

// archiveJobData: sync with localStorage
const archiveJobDataStorage = JSON.parse(localStorage.getItem("archiveJobData"));
export const archiveJobData = writable(notAnArray(archiveJobDataStorage) ? starterArchiveData : archiveJobDataStorage);
archiveJobData.subscribe(value => {
  const sortedJobs = reorderData(value)
  localStorage.setItem("archiveJobData", value ? JSON.stringify(sortedJobs) : JSON.stringify(starterArchiveData));
})
// yearlyArchiveData: sync with localStorage
const yearlyArchiveStorage = JSON.parse(localStorage.getItem("yearlyArchiveData"));
export const yearlyArchiveData = writable(notAnArray(yearlyArchiveStorage) ? starterYearlyArchive : yearlyArchiveStorage);
yearlyArchiveData.subscribe(value => {
  localStorage.setItem("yearlyArchiveData", value ? JSON.stringify(value) : JSON.stringify(starterYearlyArchive));
})


function notAnArray(x) {
  if (!Array.isArray(x)) {
    return true
  }
  return false;
}
function reorderData(jobData) { // reorder jobs by 1) Date, 2) Name
  jobData.sort((j1, j2) => {
    if (j1.date < j2.date) {
      return -1;
    } else if (j1.date > j2.date) {
      return 1
    } else {
      if (j1.name < j2.name) {
        return -1
      } else if (j1.name > j2.name) {
        return 1
      }
    }
    return 0;
  })
  jobData.forEach((job, i) => { // reset IDs
    job.id = i;
  })
  return jobData;
}
